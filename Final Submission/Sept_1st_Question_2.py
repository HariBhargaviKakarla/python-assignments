def rev_filename(file_name):
    rev_name = ".txt"
    for char in file_name:
        rev_name = char + rev_name
    return rev_name

with open("D:\Workspace\September -1st\myFile.txt") as f1:
    a = f1.readlines()
    print(a)
    rev_data = []
    for i in range(1,len(a)+1):
        sentc = a[-i]
        rev_sentc = ""
        if i > 1:
            sentc = sentc[:-1]
        for char in sentc:
            rev_sentc = char + rev_sentc
        rev_sentc  = rev_sentc + "\n"
        rev_data.append(rev_sentc)
    print(rev_data)
    
file_name = "myFile"
rev_name = rev_filename(file_name)
with open("D:\Workspace\September -1st"+rev_name, "w") as f2:
    for each_sentc in rev_data:
        f2.write(each_sentc)

