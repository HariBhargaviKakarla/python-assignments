open_list = ["[","{","("]
close_list = ["]","}",")"]

def checkParenthesis(st):
    stack = []
    for i in st:
        if i in open_list:
            stack.append(i)
        elif i in close_list:
            pos = close_list.index(i)
            if ((len(stack) > 0) and
                (open_list[pos] == stack[len(stack)-1])):
                stack.pop()
            else:
                return "Unbalanced"
    if len(stack) == 0:
        return "Balanced"
    else:
        return "Unbalanced"

file = open("program.js", "r")

st=""
for line in file:
    for ch in line:
        if ch in open_list or ch in close_list:
            st += ch
file.close()

print(checkParenthesis(st))