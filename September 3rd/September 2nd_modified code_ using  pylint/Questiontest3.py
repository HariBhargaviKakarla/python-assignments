'''
Python3 code to Check for balanced parentheses in a data read from the file
'''
openbraces = ["[", "{", "("]
closingbraces = ["]", "}", ")"]

def isbalanced(inp):
    '''
function to check whether the paranthesis are balanced or not
Parameters
----------
str1 : string

Returns
------
String
    "Balanced" if paranthesis are balanced
    "Unbalanced" if paranthesis are unbalanced
'''
    stack = []
    for i in inp:
        if i in openbraces:
            stack.append(i)
        elif i in closingbraces:
            pos = closingbraces.index(i)
            if ((len(stack) > 0) and
                    (openbraces[pos] == stack[len(stack)-1])):
                stack.pop()
    if len(stack) == 0:
        return "Balanced"
    return "Unbalanced"


filename = input()

with open(filename, "r") as file:
    data = file.read()
    file.close()

input1 = str(data)
print(input1, "-", isbalanced(input1))

