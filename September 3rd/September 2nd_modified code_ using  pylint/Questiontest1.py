'''
Object oriented program to implement a linked list.
The Linked list class Contains the following member functions.
  -> Insert an element at beginning
  -> Insert an element at end
  -> Insert an element at nth position
  -> Delete an element at beginning
  -> Delete an element at end
  -> Delete an element at nth position
  -> Display the list
'''


class Node:
    '''
          Creats a node for the elements in the lists

          ...

          Methods
          -------
          __init__(self, data)
                  Initialize an instance with attribute data as data and next as
                          None
    '''

    def __init__(self, data):
        self.data = data
        self.next = None


class Linkedlist:
    '''
          class to handling and creating linked list

          ...

          Methods
          -------
          __init__(self)
                  Initialize an Instance(Linked List) with head as None

    insert_at_beginning(self,data)
      insert an element with data at the start

    insert_at_end(self,data)
      insert an element with data at the end

          insert_at_position(self, pos, data)
                  insert an element with data at the given position

          delete_at_beginning(self)
      removes the elements at the start

    delete_at_end(self)
      removes the elements at the end

    delete(self, pos)
                  removes the elements at the given position

          print(self)
                  prints elements of the linked list in a formated form

          Raises
          ------
          ValueError
                  when delete is called on empty linked list

          IndexError
                  when invalid postion is given

          Example
          -------
          For Inserting elements
                  >>> from linkedlist.singleLinkedList import LinkedList
                  >>> linkedList = Linkedlist()
                  >>> linkedList.insert_beginning(10)
                  >>> linkedList.insert(1, 20)
                  >>> linkedList.insert_at_end(30)
                  >>> linkedList.display
                          [10, 20 , 30]

          For removing elements
                  >>> linkedList.display()
                          [10, 20 ,30]
                  >>> linkedList.delete_at_beginning()
                  >>> linkedList.delete_at_end()
                  >>> LinkedList.display()
                          [20]
          '''

    def __init__(self):
        self.head = None

    @staticmethod
    def type_check(valueToCheck):
        '''
        checks if valueToCheck is int values

        ...

        Parameters
        ----------
        valueToCheck : int
          The value which to be check if it is int or not

        Returns
        -------
        userValue : bool
          if value is int or not

        '''
        try:
            # checks valueToCheck input is int and validate lower lowercap
            # if type(valueToCheck) is not int:
            if not isinstance(valueToCheck,int):
                raise ValueError
            return True
        except ValueError:
            return False

    def insert_at_beginning(self, data):
        '''
                    adds element at the begining of the linked list
                    ...
                    Parameters
                    ----------
                    data : Any
                            data that should be prepended to the linked list

                    '''
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node

    def insert_at_end(self, data):
        '''
                    adds element at the end of the linked list
                    ...
                    Parameters
                    ----------
                    data : Any
                            data that should be added at end of the linked list
        '''
        new_node = Node(data)
        temp = self.head
        while temp.next:
            temp = temp.next
        temp.next = new_node

    def insert_at_position(self, pos, data):
        '''
                    Insert a element of value at the position (0 based indexing) and
                            support negative indexing

                    Parameters
                    ----------
                    pos : int
                            The position at which the element is to be inserted (Zero based index)
                    data : Any
                            the data which is to be inserted

                    Raises
                    ------
                    IndexError
                            when invalid postion is given

                    Examples
                    --------
                    using 0 based indexing
                    >>> linkedList = Linkedlist()
                    >>> linkedList.insert(0, 10)
                    >>> linkedList.insert(1, 20)
                    >>> linkedList.insert(1, 30)
                    >>> linkedList.display()
                            [10, 30 , 20]

                    using -ve based indexing
                    >>> linkedList = LinkedList()
                    >>> linkedList.insert(0, 10)
                    >>> linkedList.insert(-1, 20)
                    >>> linkedList.insert(-1, 30)
                    >>> linkedList.display()
                            [10, 20 , 30]

                    '''
        # dealing with position argment type
        if self.type_check(pos):
            pass
        else:
            raise IndexError("Position argument should be a integer type")

        new_node = Node(data)
        temp = self.head
        i=1
        while i < (pos-1):
            temp = temp.next
            i+=1
        new_node.data = data
        new_node.next = temp.next
        temp.next = new_node

    def delete_at_beginning(self):
        '''
                    removes the first element at the stating of the linked list

                    Raises
                    ------
                    ValueError
                            when delete is called on empty linked list
                    '''
        # when linked list is empty
        if self.head is None:
            raise ValueError(
                "LinkedList is empty, doesn't have elements to remove")

        temp = self.head
        self.head = temp.next
        temp.next = None

    def delete_at_end(self):
        '''
                    removes the first element at the end of the linked list
        '''
        prev = self.head
        temp = self.head.next
        while temp.next is not None:
            temp = temp.next
            prev = prev.next
        prev.next = None

    def delete_at_position(self, pos):
        '''
                    removes the element at the given index(zero based indexing)
                    ...
                    prints proper error when the argument is not valid

                    Parameters
                    ----------
                    pos : int
                            the index of the element which is to be removed

                    Raises
                    ------
                    IndexError
                            when invalid position is given

                    Examples
                    --------
                    >>> linkedList.display()
                            [10, 20 ,30]
                    >>> linkedList.delete(0)
                    >>> linkedList.delete(1)
                    >>> print(linkedList.getList())
                            [20]

                    Example-2
                    >>> linkedList.display()
                            [10, 20 ,30]
                    >>> linkedList.delete(-1)
                    >>> linkedList.delete(-1)
                    >>> print(linkedList.getList())
                            [10]

                    '''
        # dealing with position argment type
        if self.type_check(pos):
            pass
        else:
            raise IndexError("Position argument should be integer type")

        prev = self.head
        temp = self.head.next
        i=1
        while i < (pos-1):
            temp = temp.next
            prev = prev.next
            i+=1
        prev.next = temp.next

    def display(self):
        '''
                    prints the currently stored linked list

                    Raises
                    ------
                    ValueError
                            when linked list is empty

                    '''
        if self.head is None:
            raise ValueError("The Linked List is empty")

        # printing element wise
        temp = self.head
        while True:
            # when end of linked list is reached
            if temp is None:
                print(None)
                return
            print(temp.data, end=' -> ')
            temp = temp.next


# Driver Program
l = Linkedlist()
n = Node(1)
l.head = n
n1 = Node(2)
n.next = n1
n2 = Node(3)
n1.next = n2
n3 = Node(4)
n2.next = n3
l.insert_at_beginning(5)
l.insert_at_end(5)
l.insert_at_position(1, 5)
l.delete_at_beginning()
l.delete_at_end()
l.delete_at_position(5)
l.display()

