def convert_to_number(input_list):
    number = ""
    for ele in input_list:
        number =  number + str(ele)
    return int(number)

def add_nums(num1, num2):
    if num1 > num2:
        for i in range(num2):
            num1 = num1+1
        return str(num1)
    else:
        for i in range(num1):
            num2 = num2+1
        return str(num2)
    
list_a = [1,2,3,4]
list_b = [8, 6]

num1 = convert_to_number(list_a)
num2 = convert_to_number(list_b)

result = add_nums(num1, num2)
result_list = []
for num in result:
    result_list.append(int(num))
print(result_list)
