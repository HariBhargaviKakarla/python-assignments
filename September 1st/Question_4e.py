n=int(input("Enter the number of key-value pairs you want to print : " ))

d = dict()

for x in range(1,n):
    d[x]=x*x


print(d) 

print("Sum of keys for the dictionary generated : ",sum(d.values()))
print("Sum of values for the dictionary generated : ",sum(d.keys()))
