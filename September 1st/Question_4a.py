import operator
d = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
sorted_d = sorted(d.items(), key=operator.itemgetter(1))
print('Dictionary in ascending order by value : ')
print(sorted_d)
sorted_d = dict( sorted(d.items(), key=operator.itemgetter(1),reverse=True))
print('Dictionary in descending order by value : ')
print(sorted_d)
