def sort_dict_by_key(d, reverse = False):
	  return dict(sorted(d.items(), reverse = reverse))
d = { 1: 1, 2: 4, 3: 9, 4: 16, 5: 25 }

print("\nSort the said dictionary by key (Ascending order):")
print(sort_dict_by_key(d))
print("\nSort the said dictionary by key (Descending order):")
print(sort_dict_by_key(d, True))