import pytest
from calc import add,divide
def test_add():
    res=add(3,4)
    assert res==7
    assert add(8,5)==13

def test_div():
    assert divide(4,2)==2
    assert divide(10,4)==2.5
    # assert divide(10,3)==3.33, "rounding works"
    # assert divide(2,0)=="division by zero error"
