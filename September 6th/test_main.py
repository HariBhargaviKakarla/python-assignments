import unittest
import main
class TestCal(unittest.TestCase):
    def test_add(self):
        self.assertEqual(main.add(10,15),25)

    def test_add_notequal(self):
        self.assertNotEqual(main.add(10,15),5)

    def test_add_True(self):
        self.assertTrue(main.add(10,15)==25,"the result is false")

    def test_add_False(self):
        self.assertFalse(main.add(10,15)==2,"the result is true")

    def test_add_Is(self):
        self.assertIs(main.add(10,15),main.add(10,15),"does not match")
    
    def test_add_IsNot(self):
        self.assertIsNot(main.add(10,15),main.add(10,55),"match")

    
    def test_add_In(self):
        self.assertIn(main.add(10,15),[25,26,27],"not in list")

    def test_add_almostEqual(self):
        self.assertAlmostEqual(main.add(10,15),25.000000001)

    def test_add_almostnotEqual(self):
        self.assertNotAlmostEqual(main.add(10,15),25.0985001)

    def test_add_greater(self):
        self.assertGreater(main.add(10,15),1)

if __name__=='__main__':
    unittest.main()