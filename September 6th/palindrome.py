def ispalindrome(data):
    if type(data) != str and type(data) != list:
        return False
    if type(data)==str:
        data=data.lower()
    rev=data[::-1]
    if data == rev:
        return True
    return False

if __name__=='__main__':
    print(ispalindrome("abcba"))
    print(ispalindrome([3,4,5,4,3]))
    print(ispalindrome("amma"))
